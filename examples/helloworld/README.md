# Hello World
## To run this project 
```console 
$ npm i 
$ bower i
$ pulp build
$ npm run start
```
## Instructions to get started
1. Create directory `helloworld`
2. Open terminal in directory and execute following command
    ```console
        $ pulp init
        $ npm init
    ```
3. Now we will install `webpack`, `webpack-dev-server` and `wepack-cli` to start development server
    ```console
        $ npm i webpack -s
        $ npm i webpack-dev-server -s
        $ npm i webpack-cli -s
    ```
4. Now create `dist` directory in root folder and inside it create `index.html`
5. Paste following code inside  `dist/index.html`
```html
   <html>
    <body>
        <script src="bundle.js"></script>
    </body>
    </html>
```
6. create `index.js` file in root directory and paste following code for now
```JavaScript
    document.write('<h1>Hello World</h1> <br/> Generated from JS');
```
7. Now create `webpack.config.js` in root directory and paste following code
```javascript
    module.exports = {
        entry: "./index.js",
        mode: 'development',
        output: {
            path: __dirname + "/dist",
            filename: "bundle.js"
        },
        watch : true
    }
  
```
8. Now open `package.json` and paste following line under `scripts:{}`
```json
       ,"start": "webpack-dev-server --content-base ./dist --host 0.0.0.0"
```
   after adding the file should look like this
```json 
    {
    "name": "helloworld",
    "version": "1.0.0",
    "description": "\"Hello World\"",
    "main": "index.js",
    "directories": {
        "test": "test"
        
    },
    "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1"
        ,"start": "webpack-dev-server --content-base ./dist --host 0.0.0.0"
    },
    "author": "Shivam Ashtikar",
    "license": "ISC",
    "dependencies": {
        "webpack": "^4.28.4",
        "webpack-dev-server": "^3.1.14"
    }
    }
```
9. Now execute `$ npm run start` in terminal. Web server should run at http://0.0.0.0:8080/

### This contents were generated from Javascript itself. Now, let's do some `Purescript`
First of all install following dependencies.
```console
    $ npm i ramda -s
    $ npm i presto-ui -s
```
 Add following line under "dependancies:" in `bower.json`
```json
 "purescript-presto-dom":"https://bitbucket.org/juspay/purescript-presto-dom.git#master"
```
Do `$ bower i`

1. open `src/Main.purs` and replace it with following lines
```purescript
    module Main where

    import Prelude

    import Effect (Effect)
    import Effect.Aff (Aff, launchAff_, makeAff, delay, Milliseconds(..))
    import Effect.Class.Console (log)
    import View.Hello as Hello
    import PrestoDOM.Core (runScreen, initUI)
    import PrestoDOM.Types.Core (Screen)

    main :: forall eff. Effect Unit
    main = do
    _ <- launchAff_ do
        _ <- makeAff (\cb -> initUI cb)
        _ <- runUI Hello.screen "2"

        pure unit

    pure unit

    runUI :: forall a r s. Screen a s r -> String -> Aff Unit
    runUI screen  txt = do
    _ <- makeAff (\cb -> runScreen screen cb)
    log $ "Completed " <> txt
```
2. Create two files `src/View/Hello.purs` and `src/Controller/Hello.purs` 
3. Paste following in `src/View/Hello.purs`
```purescript
    module View.Hello where

    import Prelude

    import Effect (Effect)
    import PrestoDOM.Elements.Elements
    import PrestoDOM.Properties
    import PrestoDOM.Types.DomAttributes
    import PrestoDOM.Types.Core (PrestoDOM, Screen)
    import Controller.Hello (Action(..), State, eval, initialState)

    screen :: forall eff. Screen Action State Unit
    screen =
        { initialState
        , view
        , eval
        }

    helloWorld :: String 
    helloWorld = "> Hello World!!"

    view :: forall w eff. (Action ->  Effect Unit) -> State -> PrestoDOM (Effect Unit) w
    view push state =
      linearLayout
        [ height MATCH_PARENT
        , width MATCH_PARENT
        , background "#000000"
        , gravity CENTER
        ]
        [ textView
            [
            height WRAP_CONTENT
            ,width WRAP_CONTENT
            ,text helloWorld
            ,color "#32CD32"
            ,textSize 28
            ] 
        ]
```
4. Paste following in `src/Controller/Hello.purs`
```purescript
    module Controller.Hello where

    import Prelude

    import PrestoDOM (exit, updateAndExit, continue, Eval)
    import PrestoDOM.Types.DomAttributes (Visibility(..))


    type State = { dummyState :: String }
    data Action = Nothing 

    initialState :: State
    initialState = { dummyState : "cool"}

    eval :: forall eff. Action -> State -> Eval Action Unit State
    eval Nothing state  = continue $ state { dummyState = "hello"}
```

5. Now edit `webpac.config.js` . It should look like this
```javascript
   module.exports = {
        entry: "./index.js",
        devtool: "source-map",
        mode: "development",
        output: {
            path: __dirname + "/dist",
            filename: "index.js",
            sourceMapFilename: "index_bundle.js.map"

        },
        watch : true
    }
```
1. Edit `index.js` . It should look like this
```javascript
    var purescript  = require('./output/Main');
    purescript.main()
```
7. `dist/index.html` should look like this
```html
    <html>
    <body>
        <!-- This is required for Presto-ui styling -->
        <style>
            * {
            padding: 0;
            margin: 0;
            box-sizing: border-box;
            letter-spacing: 1.2;
            }
    
            input {
            }
    
            #content {
                width: 100%;
                height: 100%;
                position: relative;
            }
        </style>
        
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro|VT323" rel="stylesheet" />

        <!-- All the views generated by presto-dom will get appended to this div -->
        <div id="content"> </div>

        <!-- js file. Most important!!!! -->
        <script src="index.js"></script>
    </body>
    </html>
```
8. Now we are ready to go. open two terminal in this project directory
   1. In terminal One execute `pulp --watch build`
   2. In terminal Two execute `npm run start` 