module View.Hello where

import Prelude

import Effect (Effect)
import PrestoDOM.Elements.Elements
import PrestoDOM.Properties
import PrestoDOM.Types.DomAttributes
import PrestoDOM.Types.Core (PrestoDOM, Screen)
import Controller.Hello (Action(..), State, eval, initialState)


screen :: forall eff. Screen Action State Unit
screen =
  { initialState
  , view
  , eval
  }

helloWorld :: String 
helloWorld = "> Hello World!!"

view :: forall w eff. (Action ->  Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  linearLayout
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , background "#000000"
    , gravity CENTER
    ]
    [ textView
        [
          height WRAP_CONTENT
          ,width WRAP_CONTENT
          ,text helloWorld
          ,color "#32CD32"
          ,textSize 28
        ]
    ]
