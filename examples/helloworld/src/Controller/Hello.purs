module Controller.Hello where

import Prelude

import PrestoDOM (exit, updateAndExit, continue, Eval)
import PrestoDOM.Types.DomAttributes (Visibility(..))


type State = { dummyState :: String }
data Action = Nothing 

initialState :: State
initialState = { dummyState : "cool"}

eval :: forall eff. Action -> State -> Eval Action Unit State
eval Nothing state  = continue $ state { dummyState = "hello"}