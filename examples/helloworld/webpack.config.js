module.exports = {
    entry: "./index.js",
    devtool: "source-map",
    mode: "development",
    output: {
        path: __dirname + "/dist",
        filename: "index.js",
        sourceMapFilename: "index_bundle.js.map"

    },
    watch : true
}